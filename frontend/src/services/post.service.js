import { API_BASE_URI } from '../constants/URLConstants';
import { authHeader } from '../helpers/auth-header';

export const postService = {
    create,
    getAll
};

function create(obj) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj)
    };

    return fetch(`${API_BASE_URI}post/create`, requestOptions).then(res => res.json())   
        .then(post => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            
            return post;
        });
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${API_BASE_URI}post/all`, requestOptions).then(res => res.json()) ;
}

