import React, { Component } from 'react';
import LoginForm from './loginForm';
import './login.css';
import { userActions } from '../../actions/user.action';
import { connect } from 'react-redux';


class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: '',
        password: '',
        submitted: false
    };
    this.submit = this.submit.bind(this);
  }

  async  submit(values) {
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
    await sleep(500);
    const { dispatch } = this.props;
    dispatch(userActions.login(values));
   };

  render() {
    return (
   <div className="login-page">    
       <h1 className="login-header">Login</h1>      
       <LoginForm onSubmit={this.submit} />  
   </div>
    );
  }
}

function mapStateToProps(state) {
  const { loggingIn } = state.authentication; 
  return {
      loggingIn
  };
}

export default connect(mapStateToProps)(Login);
