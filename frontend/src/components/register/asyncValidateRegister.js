import { authHeader } from '../../helpers/auth-header';
import { API_BASE_URI } from '../../constants/URLConstants';

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

const registerUsernameValidate =  (values) => {
  return sleep(1000).then(async () => {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };
   
    const res = await fetch(`${API_BASE_URI}user?email=${values.email}`, requestOptions).then(res => res.json())
   
    if (!(res && res.error && res.error ==='Not Found')) {
        throw { email: 'That email already exist' };
    }
  });
};

const asyncValidate = registerUsernameValidate;


export default asyncValidate;
