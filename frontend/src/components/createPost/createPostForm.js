import React from 'react'
import { Field, reduxForm } from 'redux-form';
import './createPostForm.css'
import { required, email } from '../../constants/ValidationConstant';
import { connect } from 'react-redux';

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className='form-field-col'>
      
        <input className={touched ? error ? "errorField": "successField": null } {...input} placeholder={label} type={type}/>
        
        {touched && ((error && <span className="error">{error}</span>) || (warning && <span>{warning}</span>))}
      
    </div>
)
  

let CreatePostForm = props => {
  const { handleSubmit, pristine, reset, submitting, loggingIn } = props
  return (
    <form onSubmit={handleSubmit} className="login-form">
      <div className="form-field-row-inline">
            <Field name="title" component={renderField} label="Title" type="text" validate={[required]} />
            <Field name="description" component={renderField} label="Description" type="text" validate={[ required ]} />
      </div>
      
        <div className="submit-block">
            <button className="primary-button" type="submit" disabled={submitting}>Publish</button>
             {loggingIn &&
                            <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                        }
            <button className="default login-reset-button" type="button" disabled={pristine || submitting} onClick={reset}>Reset</button>
            {/* <button type="submit" className="primary-button" type="submit">Submit</button> */}
        </div>
      
      
    </form>
  )
}


CreatePostForm = reduxForm({
  // a unique name for the form
  form: 'post',
})(CreatePostForm)

export default connect()(CreatePostForm);