import React, { Component } from 'react';
import CreatePostForm from './createPostForm';
import './createPostForm.css';
import { postActions } from '../../actions/post.action';
import { connect } from 'react-redux';
import './createPost.css'
import { Link } from 'react-router-dom'

class CreatePost extends Component {

  constructor(props) {
    super(props);
    this.state = {
        email: '',
        password: '',
        submitted: false
    };
    this.submit = this.submit.bind(this);
  }

  async  submit(values) {      
    const { user } = this.props;
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
    await sleep(500);
    let newValues = {...values, email: user.data.email, name: user.data.name}
    const { dispatch } = this.props;
    dispatch(postActions.create(newValues));
   };

  render() {
    return (
   <div className="create-post-page">
      <div className="right-side-stick-menu">
                    <Link to="/">Search</Link>
                    <Link to="/home">Logout</Link>
       </div>
       <h1 className="login-header">Create Post</h1>      
       <CreatePostForm onSubmit={this.submit} />  
   </div>
    );
  }
}

function mapStateToProps(state) {
    const { authentication } = state;
    const { user } = authentication;
    return {
        user,
    };
}

export default connect(mapStateToProps)(CreatePost);
