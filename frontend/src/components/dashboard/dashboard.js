import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../../actions/user.action';
import { postActions } from '../../actions/post.action';
import './dashboard.css'

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state={
            search:null
        };
    }

    componentDidMount() {
        this.props.dispatch(postActions.getAll());        
    }

    searchPost=(event)=>{
        let keyword = event.target.value;
        this.setState({search:keyword})
    }

    render() {
        const { user, users } = this.props;
        const elementStyle ={
            border:'solid',
            borderRadius:'10px',
            position:'relative',
            left:'10vh',
            height:'3vh',
            width:'20vh',
            marginTop:'5vh',
            marginBottom:'10vh'
          }
          const posts = users.items && users.items.filter((data)=>{
            if(this.state.search == null)
                return data
            else if(data.title.toLowerCase().includes(this.state.search.toLowerCase())){
                return data
            }
          })
        return (
            <div className="dashboard-page">
                <div className="right-side-stick-menu">
                    <Link to="/create-post">Create Post</Link>
                    <Link to="/home">Logout</Link>
                </div>
                <h1>Hi {user.data.name}! Please check all post</h1> 
                <div className="search-box">
      <input type="text" placeholder="Search Post" onChange={(e)=>this.searchPost(e)} />
     
      </div>
                {users.loading && <em>Loading Posts...</em>}
                {users.error && <span className="text-danger">ERROR: {users.error}</span>}
                {users.items &&
                    <div className="custom-card">
                        {posts.map((user, index) =>
                            <div className="custom-card-content" key={user.id}>
                                <div className="custom-card-inner" >
                                <h2>{user.title}</h2>
                                <p>{user.description}</p>
                                <div className="custom-card-footer">
                                    <p>Created By <strong>{user.name}</strong></p>
                                </div>
                                </div>
                            </div>
                        )}                        
                    </div>
                }
                {posts && posts.length === 0 ? <p>No data found</p>: null}              
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;    
    return {
        user,
        users
    };
}

export default connect(mapStateToProps)(Dashboard);