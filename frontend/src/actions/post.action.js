import { userConstants } from '../constants/ActionTypes';
import { alertActions } from './alert.action';
import { postService } from '../services/post.service';

export const postActions = {
    create,
    getAll,
};

function create(obj) {
    return dispatch => {
        dispatch(request(obj));

        postService.create(obj)
            .then(
                user => { 
                    dispatch(success(user));
                    dispatch(alertActions.success(user.message));
                    setTimeout(() => {
                        dispatch(alertActions.clear());
                        window.location.reload(true);
                    }, 3000)
                    // window.location.reload(true);
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error));
                    setTimeout(() => {
                        dispatch(alertActions.clear());
                    }, 3000)
                }
            );
    };

    function request(user) { return { type: userConstants.REGITSER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGITSER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGITSER_FAILURE, error } }
}


function getAll() {
    return dispatch => {
        dispatch(request());

        postService.getAll()
            .then(
                users => dispatch(success(users)),
                error => { 
                    dispatch(failure(error));
                    dispatch(alertActions.error(error))
                }
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}