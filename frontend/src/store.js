import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import rootReducer from './reducers/reducers';
import { createLogger } from 'redux-logger';
import axe from 'react-axe';

export const history = createHistory()

const logger = createLogger();

const initialState = {}
const enhancers = []
const middleware = [
  thunk,
  logger,
  routerMiddleware(history)
]

if (process.env.NODE_ENV === 'development') {
  if (window.chrome) {
        axe(React, ReactDOM, 1000);
  }
  const devToolsExtension = window.devToolsExtension

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

 const store = createStore(
  rootReducer,
  initialState,
  composedEnhancers
)

export default store
