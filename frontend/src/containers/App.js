import React, { Component } from 'react';
import './App.css';
import Routes from '../routes';
import { alertActions } from '../actions/alert.action';
import { history } from '../helpers/history';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class App extends Component {

  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
        // clear alert on location change
        dispatch(alertActions.clear());
    });
  }

  render() {
    const { alert } = this.props;
  return (
    <div className="App">
      <header className="smarterp-header">
      <img className="logo" src="https://www.smarterp.com/wp-content/uploads/2016/10/smart-logo-1.png" alt="Logo" />      
      </header>
      <div>
      {alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
       }
        <Routes history={history} />
      </div>
    </div>
  );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
      alert
  };
}

export default connect(mapStateToProps)(App);

