import { postConstants } from '../constants/ActionTypes';


export function post(state = {}, action) {
    switch (action.type) {
    case postConstants.POST_REQUEST:
        return {
        loggingIn: true,
        user: action.user
        };
    case postConstants.POST_SUCCESS:
        return {
        loggedIn: true,
        user: action.user
        };
    case postConstants.POST_FAILURE:
        return {};
    default:
        return state
    }
}