import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import { alert } from './alert.reducer';
import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { post } from './post.reducer';

 const rootReducer = combineReducers({
    form: formReducer,
    alert: alert,
    authentication, authentication,
    users, users,
    post: post
});

export default rootReducer;
