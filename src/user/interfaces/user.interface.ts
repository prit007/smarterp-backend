export interface User {
    readonly name: string;
    readonly email: string;
    password: string;
    readonly created_at: Date;
}