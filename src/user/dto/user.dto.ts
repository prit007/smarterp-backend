import { ApiProperty } from '@nestjs/swagger';
import { config, UserRole } from 'src/config';

export class CreateUserDTO {
    @ApiProperty()
    readonly name: string;
    
    @ApiProperty()
    readonly email: string;

    @ApiProperty()
    password: string;

    // // @ApiProperty({ enum: config.userRoles})
    // role: UserRole;   
}

export class CreateLoginDTO {   
    @ApiProperty()
    readonly email: string;

    @ApiProperty()
    readonly password: string;
}