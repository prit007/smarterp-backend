import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Posts } from './entity/posts.entity';
import { UpdateResult, DeleteResult } from  'typeorm';

@Injectable()
export class PostService {
constructor(
    @InjectRepository(Posts)
    private postRepository: Repository<Posts>,
  ) {}

  async findAll(): Promise<Posts[]> {
    return await this.postRepository.find();
  }

  async create(post: Posts): Promise<Posts> {
    return await this.postRepository.save(post);
  }

  async update(id: number, post: Posts): Promise<UpdateResult> {
    return await this.postRepository.update(id, post);
  }

  async findOne(id: any): Promise<Posts> {
    return await this.postRepository.findOne(id);
  }

  async remove(id: any): Promise<DeleteResult> {
    return await this.postRepository.delete(id);
  }

}