import { Controller, Get, Res, Body, Post, NotFoundException, HttpStatus, Query, UseGuards, Request, Delete, Put, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { PostService } from './post.service';
import { Posts } from './entity/posts.entity';
import { DeleteResult, UpdateResult } from 'typeorm';


@Controller('post')
export class PostController {

constructor(private readonly postService: PostService) {}  
    
@Post('/create')    
async addCustomer(@Res() res, @Body() userData: any) {
    const lists = await this.postService.create(userData);
    return res.status(HttpStatus.OK).json({
        message: "User has been created successfully",
        success: true,
        data: lists
     });
 }

@Get('all')
async findAll(@Res() res) {
    const lists = await this.postService.findAll();
    return res.status(HttpStatus.OK).json(lists);
}

@Get()
async findByEmail(@Res() res, @Query('email') id: string) {
    const lists = await await this.postService.findOne({email: id});
    if (!lists) throw new NotFoundException('Email does not exist!');
    return res.status(HttpStatus.OK).json(lists);
}

@Delete('/delete')
async delete(@Res() res, @Body() userData: Posts) {
    const lists: Posts = await this.postService.findOne({email: userData.email});
    if (!lists) {
        throw new NotFoundException('Post does not exist');
    } else {
        const deleteList: DeleteResult = await this.postService.remove(lists.id);
        return res.status(HttpStatus.OK).json({
            message: 'Post has been deleted',
            deleteList
        })
    }
 }

}
